cmake_minimum_required(VERSION 3.5)
project(robot_protocols)

find_package(Protobuf REQUIRED)
include_directories(${Protobuf_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_BINARY_DIR})

protobuf_generate_cpp(PROTO_SRC_CC PROTO_HDR_CC ball_vision.proto frame.proto 
    slpp.proto ball_launcher.proto)

add_library(rob_proto SHARED
  ${PROTO_SRC_CC}
  )
target_link_libraries(rob_proto
  ${Protobuf_LIBRARIES}
  protobuf
  )

find_program(PYTHON "python")

if (PYTHON)
  protobuf_generate_python(PROTO_PY ball_vision.proto frame.proto slpp.proto
      ball_launcher.proto)
  configure_file(setup.py.in setup.py)
  add_custom_target(robpy_proto ALL
    SOURCES ${PROTO_PY}
    )
endif()

#Compile with C++11 support only
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

install(TARGETS rob_proto DESTINATION lib)
install(FILES ${PROTO_HDR_CC} DESTINATION include/rob_proto)
#install(DIRECTORY include/ball_tracking DESTINATION include)

